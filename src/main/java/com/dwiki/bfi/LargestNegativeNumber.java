package com.dwiki.bfi;

public class LargestNegativeNumber {
    public Integer solution(Integer[] numbers){
        Integer result = -1;
        for (int i = 0; i < numbers.length ; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if(numbers[i] < 0){
                    if (numbers[j] == result){
                        result--;
                    }
                    else {
                        continue;
                    }
                }
                else {
                    continue;
                }
            }
        }
        return result;
    }
}
