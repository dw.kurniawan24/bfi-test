package com.dwiki.bfi;

import com.fasterxml.jackson.databind.ObjectMapper;
import info.debatty.java.stringsimilarity.Levenshtein;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CitiesSearchMachine {
    public List<String> searchEngine(String input) throws IOException {
        List<String> listCity = new ArrayList<String>();
        Levenshtein levenshtein= new Levenshtein();
        ObjectMapper objectMapper=new ObjectMapper();
        String country = "ID";
        URL url=new URL("https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json");

        City[] cities=objectMapper.readValue(url,City[].class);
        for (City c:cities
        ) {
            if(c.getCountry().equals(country)&&levenshtein.distance(c.getName(),input)<=5){
                listCity.add(c.getName());
            }
        }

        return listCity;
    }
}
