package com.dwiki.bfi;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CitiesSearchMachineTest {
    @Test
    void TestSearchMachine() throws IOException {
        CitiesSearchMachine csm = new CitiesSearchMachine();
        HashMap<String,Integer> tests = new HashMap<String, Integer>();
        
        tests.put("pulxu pqnjzng",1);

        tests.forEach((input, outputSize) -> {
            try {
                assertEquals(csm.searchEngine(input).size(), outputSize);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}